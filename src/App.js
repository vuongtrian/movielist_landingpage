import React from "react";
import "./App.css";
import MovieExercise from './MovieExercise';

function App() {
  return (
    <div className="App">
      <MovieExercise/>
    </div>
  );
}

export default App;
